import React from 'react'
import { Container, Table, Button, Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class FillBoard extends React.Component{
  constructor(props){
    super(props)
    const { board, drinkAmount, shotAmount, name } = this.props.location.state
    this.state = {
      board,
      drinkAmount,
      shotAmount,
      name,
    }
  }

  setTile = (yIndex, xIndex, type) => {
    // Set tile at given location to "S", "D", etc
    let tempBoard = this.state.board
    tempBoard[ yIndex ][ xIndex ] = type
    this.setState({ board: tempBoard })
    if (type === 'S') {
      this.setState({ shotAmount: this.state.shotAmount - 1})
    } else if (type === 'D') {
      this.setState({ drinkAmount: this.state.drinkAmount - 1})
    }
  }

  submitBoard = () => {
    fetch('/fillBoard',{
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({ board: this.state.board, name: this.state.name })
    })
    .then( (response) => {
      return response.json()
    })
    .then(jsonResponse => {
      this.props.history.push({
        pathname: '/game',
        state: { game: jsonResponse, name: this.state.name }
      })
    })
  }

  render() {
  const { board, drinkAmount, shotAmount, name } = this.state
  return (
    <Container>
      <h1>Choose your drinks and shots, { name }.</h1>
      <h2>{ shotAmount } shots to select. <FontAwesomeIcon icon="skull-crossbones" spin /></h2>
      <h2><FontAwesomeIcon icon="beer" /> { drinkAmount } drinks to select.</h2>
      <Container>
        <Table dark>
          <thead>
            <tr>
              <th>#</th>
              { board[ 0 ].map((column, index) => <th key={ `column-header ${ index + 1 }` }>{ index + 1 }</th>) }
            </tr>
            { board.map((row, yIndex) =>
              <tr key={ `row ${ yIndex }` }>
                <th scope="row" key={ `row-header ${ yIndex + 1 }` }>{ yIndex + 1 }</th>
                { row.map((element, xIndex) =>
                  <td className="table-buttons" key={ `cell ${ yIndex } - ${ xIndex }` }>
                    { board[ yIndex ][ xIndex ] ?
                      <div>
                        { board[ yIndex ][ xIndex ] }
                      </div> :
                      <div>
                        <Col className="hit">
                          <Button onClick={ () => this.setTile( yIndex, xIndex, "S" )}>
                            SHOT
                          </Button>
                        </Col>
                        <Col className="nailed-them">
                          <Button onClick={ () => this.setTile( yIndex, xIndex, "D" )}>
                            DRINK
                          </Button>
                        </Col>
                      </div>
                    }
                  </td>
                ) }
              </tr>
            )}
          </thead>
        </Table>
        <Col sm={{ offset: 10 }}  className="party-on">
          <Button onClick={ this.submitBoard }>Submit</Button>
        </Col>
      </Container>
    </Container>
    )
  }
}
