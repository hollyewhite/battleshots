import React, { Component } from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBeer, faSkullCrossbones, faShip, faBomb } from '@fortawesome/free-solid-svg-icons'
import './App.css'
import Routes from './Routes'

library.add(faBeer, faSkullCrossbones, faShip, faBomb)

class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes />
      </div>
    );
  }
}

export default App
