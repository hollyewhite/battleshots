import React from 'react'
import {
  Container, Form, FormGroup,
  Label, Input, Col, Button
} from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './'

export default class FormInput extends React.Component {
  constructor(props)  {
    super(props)
    this.state ={
      name: '',
      shotAmount: '',
      drinkAmount: '',
      gameWidth: '',
      gameHeight: '',
    }
  }

  handleChange = (event) => {
    const { target } = event
    const { name, value } = target
    this.setState({ [ name ]: value })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    console.log(this.state)
    fetch('/initialize',{
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify(this.state), // body data type must match "Content-Type" header
    })
    .then( (response) => {
      return response.json()
    })
    .then(jsonResponse => {
      this.props.history.push({
        pathname: '/fill',
        state: jsonResponse,
      })
    })
  }

  render() {
    const { name, shotAmount } = this.state
    return (
      <Container className="margin-pad-me">
        <h1>Welcome to Battleshots</h1>
        <div className="ship">
          <FontAwesomeIcon icon="bomb" />
          <FontAwesomeIcon icon="ship" pulse />
          <FontAwesomeIcon icon="beer" />
        </div>
        <h3>Fill out the inputs below to get your game started.</h3>
        <Container className="form-container">
          <Form onSubmit={ this.handleSubmit }>
            <FormGroup row>
              <Label sm={ 2 } for="name">Name</Label>
              <Col sm={ 10 }>
                <Input
                  type="text"
                  name="name"
                  id="name"
                  placeholder="Type yo name here, fool."
                  value={ name }
                  onChange={ this.handleChange }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={ 2 } for="shotAmount">How many shots?</Label>
              <Col sm={ 10 }>
                <Input
                  type="number"
                  name="shotAmount"
                  id="shotAmount"
                  placeholder="shots! shots! shots!"
                  value={ shotAmount }
                  onChange={ this.handleChange }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={ 2 } for="drinkAmount">How many drinks?</Label>
              <Col sm={ 10 }>
                <Input
                  type="number"
                  name="drinkAmount"
                  id="drinkAmount"
                  placeholder="1, 2, 10? Who's counting?"
                  value={ this.state.drinkAmount }
                  onChange={ this.handleChange }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={ 2 } for="drinkAmount">How many rows?</Label>
              <Col sm={ 10 }>
                <Input
                  type="number"
                  name="gameWidth"
                  id="gameWidth"
                  placeholder="the bigger the better..."
                  value={ this.state.gameWidth }
                  onChange={ this.handleChange }
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={ 2 } for="gameHeight">How many Columns?</Label>
              <Col sm={ 10 }>
                <Input
                  type="number"
                  name="gameHeight"
                  id="gameHeight"
                  placeholder="Again, the bigger the better..."
                  value={ this.state.gameHeight }
                  onChange={ this.handleChange }
                />
              </Col>
            </FormGroup>
            <FormGroup check row>
            <Col sm={{ offset: 10 }} className="party-on">
              <Button>Let's Party...</Button>
            </Col>
          </FormGroup>
          </Form>
        </Container>
      </Container>
    )
  }
}
