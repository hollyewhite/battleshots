import React from 'react'
import { Container, Table, Button, Col } from 'reactstrap'

export default class GameBoard extends React.Component{
  constructor(props){
    super(props)
    const { game, name } = this.props.location.state
    const opponent = Object.keys(game).filter((player) => player && (player !== name))[0]
    const opponentWidth = game[ opponent ].board.length
    const opponentHeight = game[ opponent ].board[0].length
    const opponentBoard = Array(opponentWidth).fill(Array(opponentHeight).fill(''))
    this.state ={
      game,
      name,
      opponent,
      opponentBoard,
    }
  }

  setMyTile = (yIndex, xIndex, type) => {
    // Set tile at given location to "S", "D", etc
    let tempBoard = this.state.game[this.state.name].board
    tempBoard[ yIndex ][ xIndex ] = type
    this.setState({ game: { ...this.state.game, [this.state.name]: { ...this.state.game[this.state.name], board: tempBoard } } })
  }

  setTheirTile = (yIndex, xIndex) => {
    let tempBoard = this.state.opponentBoard
    fetch('/testPosition',{
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({ name: this.state.opponent, xIndex, yIndex })
    })
    .then(response => response.json())
    .then(JSONresponse => {
      const tempRow = tempBoard[ yIndex ].slice()
      tempRow[ xIndex ] = JSONresponse ? `${ JSONresponse }-hit` : 'nothing'
      tempBoard[ yIndex ] = tempRow
      this.setState({ opponentBoard: tempBoard})
    })
  }

  renderMyBoardTile = (tileText, yIndex, xIndex) => {
    switch (tileText) {
      case 'S':
        return (
          <Col className="hit">
            <Button onClick={ () => this.setMyTile( yIndex, xIndex, 'S-hit' )}>
              SHOT
            </Button>
          </Col>
        )
      case 'D':
        return (
          <Col className="nailed-them">
            <Button onClick={ () => this.setMyTile( yIndex, xIndex, 'D-hit' )}>
              DRINK
            </Button>
          </Col>
        )
      case 'S-hit':
        return <div>Shoot that boi</div>
      case 'D-hit':
        return <div>Sink that drink</div>
      default:
        return <div>Nada Here</div>
    }
  }

  renderTheirBoardTile = (tileText, yIndex, xIndex) => {
    switch (tileText) {
      case 'S-hit':
        return <div>You sunk their shotty ship</div>
      case 'D-hit':
        return <div>You sunk their drinky ship</div>
      case 'nothing':
        return <div>You got bupkiss</div>
      default:
        return (
          <Col className="hit">
            <Button onClick={ () => this.setTheirTile( yIndex, xIndex )}>
              FIRE
            </Button>
          </Col>
        )
    }
  }

  render() {
    const { game, name, opponentBoard } = this.state
    const myBoard = game[ name ].board
    return (
      <Container>
        <h1>Good Luck</h1>
        <Container>
          <h3>Their Board</h3>
          <Table dark>
            <thead>
              <tr>
                <th>#</th>
                { opponentBoard[0].map((column, index) => <th key={ `column-header ${ index + 1 }` }>{ index + 1 }</th>) }
              </tr>
              { opponentBoard.map((row, yIndex) =>
                <tr key={ `row ${ yIndex }` }>
                  <th scope="row" key={ `row-header ${ yIndex + 1 }` }>{ yIndex + 1 }</th>
                  { row.map((element, xIndex) =>
                    <td className="table-buttons" key={ `cell ${ yIndex } - ${ xIndex }` }>
                      { this.renderTheirBoardTile(opponentBoard[ yIndex ][ xIndex ], yIndex, xIndex) }
                    </td>
                  ) }
                </tr>
              )}
            </thead>
          </Table>
        </Container>
        <Container>
          <h3>Your Board</h3>
          <Table dark>
            <thead>
              <tr>
                <th>#</th>
                { myBoard[ 0 ].map((column, index) => <th key={ `column-header ${ index + 1 }` }>{ index + 1 }</th>) }
              </tr>
              { myBoard.map((row, yIndex) =>
                <tr key={ `row ${ yIndex }` }>
                  <th scope="row" key={ `row-header ${ yIndex + 1 }` }>{ yIndex + 1 }</th>
                  { row.map((element, xIndex) =>
                    <td className="table-buttons" key={ `cell ${ yIndex } - ${ xIndex }` }>
                      { this.renderMyBoardTile(myBoard[ yIndex ][ xIndex ], yIndex, xIndex) }
                    </td>
                  ) }
                </tr>
              )}
            </thead>
          </Table>
        </Container>
      </Container>
    )
  }
}
