import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import FormInput from './InputForm'
import GameBoard from './GameBoard'
import FillBoard from './FillBoard'
import AppliedRoute from './AppliedRoute'

export default () =>
  (
    <BrowserRouter>
      <Switch>
          <AppliedRoute path="/" exact component={ FormInput } />
          <AppliedRoute path="/game" exact component={ GameBoard } />
          <AppliedRoute path="/fill" exact component={ FillBoard } />
      </Switch>
    </BrowserRouter>
  );
