const game = {}

const createBlankBoard = (width, height) => {
  return Array(width).fill(Array(height).fill(''))
}

const setUpGame = ({ name, gameWidth, gameHeight, drinkAmount, shotAmount }) => {
  game[ name ] = {}
  game[ name ].drinkAmount = parseInt(drinkAmount, 10)
  game[ name ].shotAmount = parseInt(shotAmount, 10)
  game[ name ].board = createBlankBoard(parseInt(gameWidth, 10), parseInt(gameHeight, 10))
  // this seems messy so maybe there is a better way
  game[ name ].name = name
  return game[ name ]
}

const fillBoard = ({ name, board }) => {
  game[ name ].board = board
  return game
}

const testPosition = ({ name, xIndex, yIndex }) => {
  return game[ name ].board[ yIndex ][ xIndex ]
}

module.exports.setUpGame = setUpGame
module.exports.fillBoard = fillBoard
module.exports.testPosition = testPosition
