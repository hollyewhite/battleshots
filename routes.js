const router = require('express').Router()

const game = require('./game')

router.get('/easteregg', (req, res) => {
  res.send('You found the easter egg!  Please give us money and whiskey.')
})

router.post('/initialize', (req, res) => {
  // Should contain { name, width, height, drinks, shots }
  res.json(game.setUpGame(req.body))
})

router.post('/fillBoard', (req, res) => {
  res.json(game.fillBoard(req.body))
})

router.post('/testPosition', (req, res) => {
  res.json(game.testPosition(req.body))
})

module.exports = router
