const express = require('express')
const bodyParser = require('body-parser')

// Set up app
const app = express()
app.use(bodyParser.json())

// Bring in routes from file
app.use('/', require('./routes'))

app.use(express.static('build'));

app.get('/*', (req, res) => {
  res.sendFile(__dirname + '/build/index.html')
})

app.listen(process.env.PORT || 8080, () => {
  console.log('Back end is up and ready to battle!')
})
